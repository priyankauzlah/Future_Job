import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primaryColor = Color(0xff4141A4);

TextStyle whiteTextStyle = GoogleFonts.poppins(color: Colors.white);